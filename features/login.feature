@Login
Feature: Login Admin

  @Positive
  Scenario Outline: Positive Login
    Given User open "https://retro-v1.herokuapp.com"
    When User input <email> and <password>
    Then Press enter
    Then User is directed to dashboard
    Then User logout
  Examples:
  | email                      | password  |
  | "melani.olivia.1@mail.com" | "123456Ad"|
  | "MELANI.OLIVIA.1@MAIL.COM" | "123456Ad"|
  | "Melani.OLIVIA.1@MAIL.COM" | "123456Ad"|

  @Negative
  Scenario Outline: Negative Login
    Given User open "https://retro-v1.herokuapp.com"
    When User input <wrong_email> and <wrong_password>
    Then Press enter
    Then Error message appear
  Examples:
  | wrong_email       | wrong_password| 
  | "admin@admin.org" | "12345"       |
  | "ADMIN@ADMIN.org" | "12345"       |
  | "admin@test.org"  | "12345678"    |