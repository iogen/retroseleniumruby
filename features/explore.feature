@explore
Feature: Explore

  Scenario Outline: Explore Admin
    Given User open "https://retro-v1.herokuapp.com"
    When User input <email> and <password>
    Then Press enter
    And User check Kandidat
    And User check Batch
    And User check Tes
    And User check Parameter Penilaian
    And User check Jadwal Interview
    And User check Penilaian Kandidat
    And User check jadwal tim
    And User check review penilaian
    
  Examples:
  | email                      | password  |
  | "melani.olivia.1@mail.com" | "123456Ad"|
  | "jono.ananta.2@mail.com"   | "123456Ad"|