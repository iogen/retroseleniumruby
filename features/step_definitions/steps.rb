Given("User open {string}") do |string|
  @browser.manage.window.maximize
  @browser.navigate.to string
end

When("User input {string} and {string}") do |email, password|
  @browser.find_element(:xpath,$email).send_keys(email)
  @browser.find_element(:xpath,$password).send_keys(password)
end

Then("Press enter") do
  @browser.find_element(:xpath,$masuk).click
  sleep(2)
end

Then("User is directed to dashboard") do
  element = @wait.until {@browser.find_element(:xpath,$topbar)}
  assert_equal("Dashboard",element.text)
end

Then("Error message appear") do
  element = @wait.until {@browser.find_element(:xpath,$error_message)}
  assert_equal("Email atau password salah",element.text)
end

Then("User open profile") do
  profile = @wait.until {@browser.find_element(:xpath,$profile_button)}
  profile.click
  name1 = @wait.until{@browser.find_element(:xpath,$profile_name)}
  savename1 = name1.text
  @browser.find_element(:xpath,$lihat_profile_button).click
  name2 = @wait.until {@browser.find_element(:xpath,$profile_name2)}
  assert_equal(savename1,name2.text)
end

Then("User logout") do
  profile = @wait.until {@browser.find_element(:xpath,$profile_button)}
  profile.click
  logout = @wait.until{@browser.find_element(:xpath,$logout)}
  logout.click
end

Then("User check Kandidat") do
  kandidat = @wait.until{@browser.find_element(:xpath,$Kandidat)}
  kandidat.click
  @wait.until{@browser.find_element(:xpath,"//*[@id='content']/div[2]/div/div/div/div/div[3]/div/div/table/tbody/tr[1]/td[2]/div/div[1]")}
  title = @browser.find_element(:xpath,$topbar).text
  @browser.save_screenshot("report/#{title}.png")
end

Then("User check Batch") do
  batch = @wait.until{@browser.find_element(:xpath,$Batch)}
  batch.click
  @wait.until{@browser.find_element(:xpath,"//*[@id='content']/div[2]/div[2]/div[1]/div/div/div[1]/div")}
  title = @browser.find_element(:xpath,$topbar).text
  @browser.save_screenshot("report/#{title}.png")
  
end

Then("User check Tes") do
  @browser.find_element(:xpath,$Tes).click
  puts @browser.find_element(:xpath,$topbar).text
end

Then("User check Parameter Penilaian") do
  @browser.find_element(:xpath,$Parameter).click
  element = @wait.until {@browser.find_element(:xpath,$topbar)}
  puts element.text
end

Then("User check Jadwal Interview") do
  @browser.find_element(:xpath,$Jadwal_interview).click
  puts @browser.find_element(:xpath,$topbar).text
end

Then("User check Penilaian Kandidat") do
  @browser.find_element(:xpath,$Penilaian_kandidat).click
  puts @browser.find_element(:xpath,$topbar).text
end

Then("User check jadwal tim") do
  @browser.find_element(:xpath,$Jadwal_tim).click
  puts @browser.find_element(:xpath,$topbar).text
end

Then("User check review penilaian") do
  @browser.find_element(:xpath,$Review_penilaian).click
  puts @browser.find_element(:xpath,$topbar).text
end

Then("User Create New Batch") do
  @browser.find_element(:xpath,$buat_baru).click
end

And("Type the batch title, city, and PIC") do
  @browser.find_element(:xpath,$input_nama_batch).send_keys("Angkatan 62")
  @browser.find_element(:xpath,$pilih_kota).click
  @browser.find_element(:xpath,$input_kota).send_keys("Jakarta")
  @browser.action.send_keys(:enter).perform
  @browser.find_element(:xpath,$PIC).click
end