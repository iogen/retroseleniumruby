# Login Element
$email = "//*[@id='app']/div/div/div/div[2]/div/div/form/div[1]/input"
$password = "//*[@id='app']/div/div/div/div[2]/div/div/form/div[2]/input"
$masuk = "//*[@id='app']/div/div/div/div[2]/div/div/form/div[3]/button"
$error_message ="//*[@id='app']/div/div/div/div[2]/div/div/form/p[1]"

#dashboard
$topbar="//*[@id='content']/div[1]/nav/span"
$Kandidat = "//*[@id='accordionSidebar']/div[1]/li[2]/a"
$Batch= "//*[@id='accordionSidebar']/div[1]/li[3]/a"
$Tes = "//*[@id='accordionSidebar']/div[1]/li[4]/a"
$Parameter = "//*[@id='accordionSidebar']/div[1]/li[5]/a"
$Jadwal_interview = "//*[@id='accordionSidebar']/div[2]/li[1]/a"
$Penilaian_kandidat = "//*[@id='accordionSidebar']/div[2]/li[2]/a"
$Jadwal_tim = "//*[@id='accordionSidebar']/div[3]/li[1]/a"
$Review_penilaian = "//*[@id='accordionSidebar']/div[3]/li[2]/a"



#profile
$profile_button="//*[@id='userDropdown']"
$email_profile = "//*[@id='content']/div[1]/nav/ul/li/div/table/tr[2]/td"
$profile_name = "//*[@id='content']/div[1]/nav/ul/li/div/table/tr[1]/td[2]"
$profile_name2 = "//*[@id='content']/div[2]/div[2]/div/div[2]/p[1]"
$lihat_profile_button = "//*[@id='content']/div[1]/nav/ul/li/div/table/tr[3]/td/a/button"
$logout ="//*[@id='content']/div[1]/nav/ul/li/div/a"

#Batch
$buat_baru = "//*[@id='content']/div[2]/div[1]/div/div/div/div/div[1]/div[2]/a"
$input_nama_batch = "//*[@id='form']/div[1]/div[1]/div/input"
$pilih_kota = "//*[@id='select-test']/div/button/div"
$input_kota = "//*[@id='select-test']/div/div/div[1]/input"
$PIC = "//*[@id='form']/div[3]/input"
$lanjut = "//*[@id='form']/div[4]/button"