# frozen_string_literal: true

require "selenium-webdriver"
require "rspec"
require "test/unit/assertions"
include Test::Unit::Assertions
require "allure-cucumber"

# Allure
Allure.configure do |c|
    c.results_directory = 'report/allure-results'
    c.clean_results_directory = true
    c.link_tms_pattern = 'https://example.org/tms/{}'
    c.link_issue_pattern = 'https://example.org/issue/{}'
  end
  
  AllureCucumber.configure do |c|
    c.tms_prefix = 'TMS_'
    c.issue_prefix = 'ISSUE_'
  end