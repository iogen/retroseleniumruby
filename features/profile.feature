@profile
Feature: Profile

  Scenario Outline: Profile check
    Given User open "https://retro-v1.herokuapp.com"
    When User input <email> and <password>
    Then Press enter
    Then User open profile
  Examples:
  | email                      | password  |
  | "melani.olivia.1@mail.com" | "123456Ad"|
  | "jono.ananta.2@mail.com"   | "123456Ad"|