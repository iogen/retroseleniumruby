@Batch
Feature: Create Batch

  Scenario: Create New Batch
    Given User open "https://retro-v1.herokuapp.com"
    When User input "melani.olivia.1@mail.com" and "123456Ad"
    Then Press enter
    And User check Batch
    Then User Create New Batch
    And Type the batch title, city, and PIC